package config;

public class Constant {
    public static final String REMOTE_DRIVER_URL = "";
    public static final String DF_SITE_URL = "";
    public static final String CANVAS_URL = "https://canvas.instructure.com/login/canvas";
    public static final String DB_HOST = "";
    public static final String DB_PORT = "";
    public static final String DB_NAME = "";
    public static final String DB_USERNAME = "";
    public static final String DB_PASSWORD = "";
}
