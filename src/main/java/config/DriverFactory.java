package config;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import utils.DataBaseConnection;
import utils.Log;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;

import static config.Constant.*;

public class DriverFactory {
    protected WebDriver webDriver;
    protected SoftAssertions softly = new SoftAssertions();

    @BeforeClass
    public void initDriver() {
        DesiredCapabilities capability = new DesiredCapabilities();
        capability.setJavascriptEnabled(true);

        String browser = System.getProperty("webdriver");

        if (browser == null || browser.equalsIgnoreCase("") || browser.equalsIgnoreCase("chrome")) {
            ChromeDriverManager.getInstance().setup();
            webDriver = new ChromeDriver(capability);
        } else {
            // Create Remote WebDriver
            try {
                capability = DesiredCapabilities.chrome();
                webDriver = new RemoteWebDriver(new URL(REMOTE_DRIVER_URL), capability);
            } catch (MalformedURLException e) {
                Log.error("#RemoteWebDriver has error: ");
                e.printStackTrace();
            }
        }

        openSite();
    }
    
    @BeforeMethod
    public void initSoftly(Method method) {
        Log.info("------------------------------------");
        Log.info("Verifying TestCase: " + method.getName());
        softly = new SoftAssertions();
    }

    @AfterClass
    public void teardown() {
        if (webDriver != null) {
            webDriver.close();
            webDriver.quit();
        }
    }

    private void openSite() {
        webDriver.get(CANVAS_URL);
        webDriver.manage().window().maximize();
        webDriver.manage().deleteAllCookies();
    }


}
