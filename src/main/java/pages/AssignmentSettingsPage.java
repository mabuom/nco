package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.WaitUtils;

public class AssignmentSettingsPage extends BasePage {
    public AssignmentSettingsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//input[@name='startDate_date'][@type='text']")
    public WebElement startDateInput;

    @FindBy(xpath = "//input[@name='dueDate_date'][@type='text']")
    public WebElement dueDateInput;

    @FindBy(xpath = "//input[@name='gradeOption']//following-sibling::span/span[@class='c-els-field__switch']")
    public WebElement gradingOptionsCheckbox;

    @FindBy(xpath = "//input[@name='PASS_FAIL']//following-sibling::span/span[@class='c-els-field__switch']")
    public WebElement passFailRadio;

    @FindBy(xpath = "//input[@name='SCORED']//following-sibling::span/span[@class='c-els-field__switch']")
    public WebElement scoredRadio;

    @FindBy(xpath = "//button//span[contains(.,'Create Assignment')]")
    private WebElement createAssignmentButton;

    @FindBy(xpath = "//button//span[contains(.,'Update')]")
    private WebElement updateAssignmentButton;

    @FindBy(xpath = "//button//span[contains(.,'Cancel')]")
    private WebElement cancelAssignmentButton;

    public void inputStartDate(String date){
        clickElement(startDateInput);
        setText(startDateInput, date);
    }

    public void inputDueDate(String date){
        clickElement(dueDateInput);
        setText(dueDateInput, date);
    }

    public void clickGradingOptions_PassFail(){
        clickElement(gradingOptionsCheckbox);
        clickElement(passFailRadio);
    }

    public void clickGradingOptions_Scored(){
        clickElement(gradingOptionsCheckbox);
        clickElement(scoredRadio);
    }

    public void clickCreateAssignment(){
        clickElement(createAssignmentButton);
        WaitUtils.waitForPageLoad(driver);
    }

    public void clickCancelAssignment(){
        clickElement(cancelAssignmentButton);
    }

    public Boolean isAssigned(String itemName){
        String xpath = "(//a[@class='ng-binding'][contains(.,'" + itemName + "')]//parent::div//parent::div)[1]//following-sibling::div//div[@name='resource-library-assigned-content']//span";
        WebElement element = driver.findElement(By.xpath(xpath));
        return isElementDisplayed(element);
    }

    public Boolean isGraded(String itemName){
        String xpath = "(//a[@class='ng-binding'][contains(.,'" + itemName + "')]//parent::div//parent::div)[1]//following-sibling::div//div[@name='resource-library-assigned-content']//span/label";
        WebElement element = driver.findElement(By.xpath(xpath));
        return isElementDisplayed(element);
    }
}
