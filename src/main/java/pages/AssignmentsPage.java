package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.WaitUtils;

public class AssignmentsPage extends BasePage{
    public AssignmentsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//span[@title='NCO']")
    private WebElement titleNCO;

    @FindBy(xpath = "//span[@class='c-els-site-nav__label ng-binding'][text()='Assignments']")
    public WebElement assignmentsLink;

    @FindBy(xpath = "//span[@class='c-els-site-nav__label ng-binding'][text()='My Resources']")
    public WebElement myResourcesLink;

    @FindBy(xpath = "//span[@class='c-els-site-nav__label ng-binding'][text()='EAQ']")
    public WebElement eaqLink;

    @FindBy(xpath = "//span[@class='c-els-site-nav__label ng-binding'][text()='Get Started']")
    public WebElement getStartedLink;

    @FindBy(xpath = "//span[@class='c-els-site-nav__label ng-binding'][text()='Help & Support']")
    public WebElement helpSupportLink;

    @FindBy(xpath = "//span[@class='c-els-site-nav__label ng-binding'][text()='Logout']")
    public WebElement logoutLink;

    public void waitForPageLoad(){
        WaitUtils.waitForElementClickable(driver, assignmentsLink);
        WaitUtils.waitForElementClickable(driver, titleNCO);
        WaitUtils.waitForElementClickable(driver, myResourcesLink);
        WaitUtils.waitForElementClickable(driver, eaqLink);
    }

    public void clickAssignments(){
        clickElement(assignmentsLink);
        WaitUtils.waitForPageLoad(driver);
    }

    public void clickMyResources(){
        waitForPageLoad();
        clickElement(myResourcesLink);
    }

    public void goToAssignment(String type, String name){
        String xpath="//label[@class='u-els-padding-left-1o4 u-els-font-size-meta ng-binding'][contains(.,'" + type + "')]" +
                "//parent::div//Following-sibling::div/a[@class='u-els-font-size-h4 ng-binding'][contains(.,'" + name + "')]";
        WebElement element = driver.findElement(By.xpath(xpath));
        clickElement(element);
    }

    public Boolean isAssignment(String type, String name){
        String xpath="//label[@class='u-els-padding-left-1o4 u-els-font-size-meta ng-binding'][contains(.,'" + type + "')]" +
                "//parent::div//following-sibling::div/a[@class='u-els-font-size-h4 ng-binding'][contains(.,'" + name + "')]";
        return isElementDisplayedBy(By.xpath(xpath));
    }

    public Boolean isAssignmentGraded(String type, String name){
        String xpath = "//div[contains(@class,'o-els-flex-layout__item u-els-width')]" +
                "//label[@class='u-els-padding-left-1o4 u-els-font-size-meta ng-binding'][contains(.,'" + type + "')]" +
                "//parent::div//following-sibling::div/a[@class='u-els-font-size-h4 ng-binding'][contains(.,'" + name + "')]" +
                "//ancestor::div[@class='c-els-tile']//label[contains(.,'Will be graded')]";
        return isElementDisplayedBy(By.xpath(xpath));
    }
}
