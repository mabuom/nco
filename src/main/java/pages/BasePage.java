package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Log;
import utils.WaitUtils;

import java.util.NoSuchElementException;

import static utils.WaitUtils.waitForElementClickable;
import static utils.WaitUtils.waitForElementPresent;

public class BasePage {
    protected WebDriver driver;

    public static void setText(WebElement element, String key) {
        for (int i = 0; i <=10 ; i++){
            try {
                if (element.isDisplayed() && element.isEnabled()) {
                    element.clear();
                    element.sendKeys(key);
                    break;
                } else {
                    Log.warn("Unable to set text on field");
                }
            } catch (StaleElementReferenceException e) {
                Log.warn("#Unable to set text on field: Element is not attached to the page document " + e.getStackTrace());
            } catch (NoSuchElementException e) {
                Log.warn("#Unable to set text on field: Element was not found in DOM " + e.getStackTrace());
            } catch (Exception e) {
                Log.warn("Unable to set text on field " + e.getStackTrace());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public String getText(WebElement element) {
        waitForElementPresent(driver, element);
        return element.getText().trim();
    }

    public void clickByJS(WebElement element) {
        waitForElementClickable(driver,element);
        try {
            if (element.isEnabled() && element.isDisplayed()) {
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
            } else {
                System.out.println("Unable to click on element");
            }
        } catch (StaleElementReferenceException e) {
            System.out.println("Element is not attached to the page DOCUMENT " + e.getStackTrace());
        } catch (java.util.NoSuchElementException e) {
            System.out.println("Element was not found in DOM " + e.getStackTrace());
        } catch (Exception e) {
            System.out.println("Unable to click on element " + e.getStackTrace());
        }
    }

    public void clickElement(WebElement element){
        waitForElementClickable(driver,element);
        for (int i = 0; i <= 30; i++) {
            try {
                element.click();
                break;
            } catch (Exception e) {
                if (i == 30) {
                    Log.info("#Can NOT click element or Can NOT find element: " + element);
                    throw e;
                } else
                    WaitUtils.sleep(1);
            }
        }
    }

    public boolean isElementDisplayed(WebElement element) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.elementToBeClickable(element));
            return element.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isElementDisplayedBy(By by) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.elementToBeClickable(by));
            return driver.findElement(by).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void selectTextInDropdownList(WebElement element, String text){
        waitForElementClickable(driver,element);
        Select select = new Select(element);
        select.selectByVisibleText(text);
    }



}
