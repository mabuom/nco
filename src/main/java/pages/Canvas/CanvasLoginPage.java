package pages.Canvas;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.BasePage;

public class CanvasLoginPage extends BasePage {

    public CanvasLoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy (id = "pseudonym_session_unique_id")
    private WebElement userNameField;

    @FindBy (id = "pseudonym_session_password")
    private WebElement passwordField;

    @FindBy (xpath = "//form[@id='login_form']//button[@class='Button Button--login']")
    private WebElement loginButton;

    public void loginCanvas(String username, String password){
        setText(userNameField, username);
        setText(passwordField, password);
        clickElement(loginButton);
        setText(passwordField, password);
        clickElement(loginButton);
    }
}
