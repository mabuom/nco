package pages.Canvas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.BasePage;
import utils.WaitUtils;

import java.util.ArrayList;

public class CanvasPage extends BasePage {
    public CanvasPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//div[@class='ic-Login__banner-title']")
    private WebElement createAccountLink;

    @FindBy(id = "signup_teacher")
    private WebElement signUpTeacher;

    @FindBy(id = "signup_student")
    private WebElement signUpStudent;

    @FindBy(xpath = "//form[@id='mktoForm_1014']//input[@id='FirstName']")
    private WebElement firsNameTeacher;

    @FindBy(xpath = "//form[@id='mktoForm_1014']//input[@id='LastName']")
    private WebElement lastNameTeacher;

    @FindBy(xpath = "//form[@id='mktoForm_1014']//input[@id='Company']")
    private WebElement companyTeacher;

    @FindBy(xpath = "//form[@id='mktoForm_1014']//select[@id='Org_Type__c']")
    private WebElement orgTypeTeacher;

    @FindBy(xpath = "//form[@id='mktoForm_1014']//input[@id='Email']")
    private WebElement emailTeacher;

    @FindBy(xpath = "//form[@id='mktoForm_1014']//input[@id='Phone_Number_2__c']")
    private WebElement phoneTeacher;

    @FindBy(xpath = "//form[@id='mktoForm_1014']//button[@class='btn']")
    private WebElement submitTeacher;

    @FindBy(xpath = "//form[@id='mktoForm_1014']//input[@id='marketingTermsofUse']")
    private WebElement checkBoxAgreeTeacher;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement loadAppButton;


    public void fillTeacherSignUp(String fName, String lName, String company, String orgType, String email, String phone){
        setText(firsNameTeacher, fName);
        setText(lastNameTeacher, lName);
        setText(companyTeacher, company);
        selectTextInDropdownList(orgTypeTeacher, orgType);
        setText(emailTeacher, email);
        setText(phoneTeacher, phone);
        clickElement(checkBoxAgreeTeacher);
        clickElement(submitTeacher);
    }

    public void goToAppElsevier(String courseName, String appName){
        String xpathCourse = "//div[@class='ic-DashboardCard__header-subtitle ellipsis'][@title='" + courseName + "']";
        String xpathApp = "//a[@class='ig-title title'][@title='" + appName + "']";

        WaitUtils.waitForElementClickableBy(driver, By.xpath(xpathCourse));
        clickElement(driver.findElement(By.xpath(xpathCourse)));
        WaitUtils.waitForElementClickableBy(driver, By.xpath(xpathApp));
        clickElement(driver.findElement(By.xpath(xpathApp)));

        clickElement(loadAppButton);

        ArrayList<String> tabs = new ArrayList<> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        WaitUtils.waitForLoadingSuccess(driver);
    }

}
