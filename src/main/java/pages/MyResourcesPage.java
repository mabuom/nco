package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.WaitUtils;

import java.util.ArrayList;

public class MyResourcesPage extends BasePage {
    public MyResourcesPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//button//span[contains(.,'Exit')]")
    public WebElement exit;

    @FindBy(xpath = "//button//span[contains(.,'Expand Image')]")
    public WebElement expandImage;

    @FindBy(xpath = "//button//span[contains(.,'Exit Skill Player')]")
    public WebElement exitSkillPlayer;

    @FindBy(xpath = "//button//span[contains(.,'Exit Patient Review Player')]")
    private WebElement exitPatientReviewPlayer;

    @FindBy(xpath = "//div[@id='top']//b")
    private WebElement titleExemplars;

    @FindBy(xpath = "//iframe")
    private WebElement iframe;

    @FindBy(xpath = "//button//span[contains(.,'New Folder')]")
    private WebElement newFolderButton;

    @FindBy(xpath = "//input[@name='folderName']")
    private WebElement folderNameInput;

    @FindBy(xpath = "//button//span[contains(.,'Create Folder')]")
    private WebElement createFolderButton;

    @FindBy(xpath = "//button//span[contains(.,'Add Resource')]")
    private WebElement addResourceButton;

    @FindBy(xpath = "//span[contains(.,'Create your first folder to begin adding and organizing your resources')]")
    private WebElement emptyFirstFolderText;

    @FindBy(xpath = "//div[@class='c-els-menu__window c-els-menu__window--right'][contains(@style,'visibility: visible')]//span[contains(.,'Visibility Settings')]")
    private WebElement visibilitySettings;

    @FindBy(xpath = "//div[@class='c-els-menu__window c-els-menu__window--right'][contains(@style,'visibility: visible')]//span[contains(.,'Assignment Settings')]")
    private WebElement assignmentSettings;

    @FindBy(xpath = "//div[@class='c-els-menu__window c-els-menu__window--right'][contains(@style,'visibility: visible')]//span[contains(.,'Rename')]")
    private WebElement rename;

    @FindBy(xpath = "//div[@class='c-els-menu__window c-els-menu__window--right'][contains(@style,'visibility: visible')]//span[contains(.,'Remove')]")
    private WebElement remove;

    @FindBy(xpath = "//form[@id='visibilitySettingsModalForm']//span[@class='u-els-bold ng-binding']")
    private WebElement folderNameTextInVisibilySetting;

    @FindBy(xpath = "//form[@id='visibilitySettingsModalForm']//input[contains(@class,'ng-not-empty')]//following-sibling::span")
    private WebElement toggleStatusOn;

    @FindBy(xpath = "//form[@id='visibilitySettingsModalForm']//input[contains(@class,'ng-empty')]//following-sibling::span")
    private WebElement toggleStatusOff;

    @FindBy(xpath = "//form[@id='visibilitySettingsModalForm']//input//following-sibling::span")
    private WebElement toggleSwitch;

    @FindBy(xpath = "//button//span[contains(.,'Done')]")
    private WebElement doneButton;

    @FindBy(xpath = "//button//span[contains(.,'Cancel')]")
    private WebElement cancelButton;

    @FindBy(xpath = "//div[@id='addResourceCatalogModal']/span[@class='c-els-modal__close']")
    private WebElement closePopupButton;



    public void clickResource(String resourceName){
        String xpath = "//a[@class='ng-binding'][contains(.,'" + resourceName +"')]";
        WaitUtils.waitForElementClickableBy(driver, By.xpath(xpath));
        clickElement(driver.findElement(By.xpath(xpath)));
        WaitUtils.waitForLoadingSuccess(driver);
    }

    public void clickResourceCatalog(String name){
        String xpath = "//div[@id='addResourceCatalogModal']//a[@class='ng-binding'][contains(.,'" + name +"')]";
        WaitUtils.waitForElementClickableBy(driver, By.xpath(xpath));
        clickElement(driver.findElement(By.xpath(xpath)));
    }

    public void clickExit(){
        clickElement(exit);
    }

    public void clickExitSkillPlayer(){
        clickElement(exitSkillPlayer);
    }

    public void clickExitPatientReviewPlayer(){
        clickElement(exitPatientReviewPlayer);
    }

    public Boolean verifyInterrelatedConcepts(String img){
        String xpath = "//img[contains(@src,'" + img + "')]";
        WaitUtils.waitForElementClickable(driver, expandImage);
        return isElementDisplayedBy(By.xpath(xpath));
    }

    public String getTitleExemplars(){
        WaitUtils.waitForFrameVisible(driver, iframe);
        driver.switchTo().frame(iframe);
        WaitUtils.waitForElementPresent(driver, titleExemplars);
        String title = getText(titleExemplars);
        driver.switchTo().parentFrame();
        return title;
    }

    public String getURLofConceptChapter(){
        ArrayList<String> tabs = new ArrayList<> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(2));
        String url = driver.getCurrentUrl();
        return url;
    }

    public void createNewFolder(String name){
        clickElement(newFolderButton);
        setText(folderNameInput, name);
        clickElement(createFolderButton);
        WaitUtils.waitForPageLoad(driver);
    }

    public Boolean isItemName(String name){
        String xpath = "//a[@class='ng-binding'][contains(.,'" + name +"')]";
        return isElementDisplayedBy(By.xpath(xpath));
    }

    public void clickVisibleIcon(String itemName){
        String xpath = "(((//a[@class='ng-binding'][contains(.,'" + itemName + "')]//parent::div//parent::div)[1])" +
                "//following-sibling::div//span)[1]";
        clickElement(driver.findElement(By.xpath(xpath)));
    }

    public Boolean isVisible(String itemName){
        WaitUtils.waitForPageLoad(driver);
        String xpath = "((((//a[@class='ng-binding'][contains(.,'" + itemName + "')]//parent::div//parent::div)[1])" +
                "//following-sibling::div//span)[1])//*[name()='svg'][contains(@class,'icon__eye-visible')]";
        WebElement element = driver.findElement(By.xpath(xpath));
        if (isElementDisplayed(element)){
            return true;
        }
        else {
            xpath = "((((//a[@class='ng-binding'][contains(.,'" + itemName + "')]//parent::div//parent::div)[1])" +
                    "//following-sibling::div//span)[1])//*[name()='svg'][contains(@class,'icon__eye-not-visible')]";
            element = driver.findElement(By.xpath(xpath));
            if (isElementDisplayed(element)){
                return false;
            }
        }
        return null;
    }

    public Boolean isEmptyFirstFolderMessage(){
        WaitUtils.waitForElementClickable(driver, newFolderButton);
        return isElementDisplayed(emptyFirstFolderText);
    }

    public void clickMenuAction(String itemName){
        String xpath = "((//a[@class='ng-binding'][contains(.,'" + itemName + "')]//parent::div//parent::div)[1])" +
                "//following-sibling::div//div[@class='ng-scope']";
        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].scrollIntoView();", driver.findElement(By.xpath(xpath)));
        clickElement(driver.findElement(By.xpath(xpath)));
    }

    public void clickVisibilitySettings(){
        clickElement(visibilitySettings);
    }

    public void clickAssignmentSettings(){
        clickElement(assignmentSettings);
    }

    public void clickRename(){
        clickElement(rename);
    }

    public void cickRemove(){
        clickElement(remove);
    }

    public String getFolderNameInVisibilitySettings(){
        return getText(folderNameTextInVisibilySetting);
    }

    public void clickDone(){
        clickElement(doneButton);
    }

    public void clickCancel(){
        clickElement(cancelButton);
    }

    public void clickToggleSwitch(){
        clickElement(toggleSwitch);
    }

    public Boolean isToggle(String text){
        if (text.equalsIgnoreCase("On")){
            return isElementDisplayed(toggleStatusOn);
        }
        else{
            return isElementDisplayed(toggleStatusOff);
        }
    }

    public void clickAddResource(){
        clickElement(addResourceButton);
    }

    public void addResource(String ressourceName){
        String xpath = "//div[@id='addResourceCatalogModal']//tr[td[contains(.,'" + ressourceName + "')]]//td//button";
        WaitUtils.waitForElementClickableBy(driver, By.xpath(xpath));
        clickElement(driver.findElement(By.xpath(xpath)));
    }

    public void clickClosePopup(){
        clickElement(closePopupButton);
    }

}
