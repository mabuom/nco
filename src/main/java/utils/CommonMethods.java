package utils;

import org.openqa.selenium.*;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.Select;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.NoSuchElementException;

import static utils.WaitUtils.waitForElementClickable;
import static utils.WaitUtils.waitForElementPresent;

public class CommonMethods {
    public static String createRandomUser() {
        String user = null;
        user = "auto_" + getCurrentTime();
        System.out.println("Random user " + user);
        return user;
    }

    public static String createRandomEmail() {
        String email = null;
        email = createRandomUser() + "@eicc.com";
        return email;
    }

    public static String createUniquePNC() {
        String pnc;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formater = new SimpleDateFormat("hhmmss");
        pnc = "06/" + formater.format(calendar.getTime()) + "Q";
        return pnc;
    }

    public static String createUniqueCRN() {
        String crn;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formater = new SimpleDateFormat("hhmmss");
        crn = "Z" + formater.format(calendar.getTime());
        return crn;
    }

    public static String getCurrentTime() {
        String time = null;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
        time = formater.format(calendar.getTime());
        return time;
    }

    public static int getCurrentHour() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.HOUR_OF_DAY);
    }

    public static int getrandom() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.MINUTE);
    }

    public static String getCurrentDate() {
        String date = null;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
        date = formater.format(calendar.getTime());
        return date;
    }

    public static String getCurrentDateWithFormat(String format) {
        String date = null;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formater = new SimpleDateFormat(format);
        date = formater.format(calendar.getTime());
        return date;
    }

    public static String getPreviousDate() {
        String day = getCurrentDate().split("/")[0];
        String prevDay = Integer.toString((Integer.parseInt(day) - 1));
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
        String date = formater.format(calendar.getTime());
        date = date.replaceAll(day, prevDay);
        return date;
    }
}


