package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBaseConnection {
    public static Connection openConnection(String host, String port, String dataBase, String userName, String password) {
        String dbInfo = host + ":" + port + "/" + dataBase;
        Log.info("Open connection to Database");

        try {
            Class.forName("org.postgresql.Driver");

        } catch (ClassNotFoundException e) {

            Log.info("Where is your PostgreSQL JDBC Driver? " + "Include in your library path!");
            e.printStackTrace();
        }

        Log.info("PostgreSQL JDBC Driver Registered!");

        Connection connection = null;

        try {
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://" + dbInfo, userName, password);

        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
        }

        if (connection != null) {
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }
        return connection;
    }
}
