package utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class GetDataFromDataBase {
    public static List<String> queryListData(Connection dataBaseConnection, String sqlStatement, String columnLabel) {
        List<String> field = new ArrayList<>();

        try {
            Statement dataQuery = dataBaseConnection.createStatement();
            ResultSet rs = dataQuery.executeQuery(sqlStatement);

            while (rs.next()) {
                try {
                    field.add(rs.getString(columnLabel));
                } catch (NullPointerException e) {
                    Log.info(e.getMessage());
                }
            }

        } catch (SQLException e) {
            Log.info(e.getMessage());
        }
        return field;
    }

    public static String queryData(Connection dataBaseConnection, String sqlStatement, String columnLabel) {
        String field=null;
        try {
            Statement dataQuery = dataBaseConnection.createStatement();
            ResultSet rs = dataQuery.executeQuery(sqlStatement);

            rs.next();
                try {
                    field = rs.getString(columnLabel);
                } catch (NullPointerException e) {
                    Log.info(e.getMessage());
                }
        } catch (SQLException e) {
            Log.info(e.getMessage());
        }
        return field;
    }
}
