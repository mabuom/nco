package utils;

import org.apache.log4j.Logger;

/**
 * Created by Tai Le on 6/29/2016.
 */
public class Log {

// Initialize Logslf4j logs

    private static Logger Log = Logger.getLogger(utils.Log.class.getName());

    public static void info(String message) {
        Log.info(message);
    }

    public static void warn(String message) {
        Log.warn(message);
    }

    public static void error(String message) {
        Log.error(message);
    }

    public static void fatal(String message) {
        Log.fatal(message);
    }

    public static void debug(String message) {
        Log.debug(message);
    }


}
