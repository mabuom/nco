package utils;

import org.json.*;

import java.io.*;
import java.util.Properties;

public class ReadFileData {

    public static Properties getProp(File file) {
        FileInputStream fileInput = null;
        String path = "src/main/resources/data/";

        try {
            fileInput = new FileInputStream(path+file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Properties prop = new Properties();

        //load properties file
        try {
            prop.load(fileInput);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return prop;
    }

    public static JSONObject readJsonFile(File file){
        String path = "src/main/resources/data/";
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(new JSONTokener(new FileReader(path+file)));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public static JSONObject getJsonObject(JSONObject jsonObjectOriginal, String key){
        JSONObject jsonObject = jsonObjectOriginal.getJSONObject(key);
        return jsonObject;
    }

}
