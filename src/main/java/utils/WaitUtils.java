package utils;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitUtils {



    public static void waitForElementPresent(WebDriver aDriver, WebElement element) {
        WebDriverWait wait = new WebDriverWait(aDriver, 60);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public static void waitForElementInvisible(WebDriver aDriver, WebElement element) {
        WebDriverWait wait = new WebDriverWait(aDriver, 10);
        wait.until(ExpectedConditions.invisibilityOf(element));
    }

    public static void waitForAjax(WebDriver driver) {
        new WebDriverWait(driver, 30).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                JavascriptExecutor js = (JavascriptExecutor) driver;
                return (Boolean) js.executeScript("return jQuery.active == 0");
            }
        });
    }

    public static void waitForLoadingSuccess(WebDriver driver){
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='c-els-loading-bar'][1]")));
    }

    public static void waitForElementClickable(WebDriver aDriver, WebElement element) {
        WebDriverWait wait = new WebDriverWait(aDriver, 60);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }
    public static void waitForElementClickableBy(WebDriver aDriver, By by) {
        WebDriverWait wait = new WebDriverWait(aDriver, 60);
        wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    public static void waitForFrameVisible(WebDriver aDriver, WebElement element) {
        WebDriverWait wait = new WebDriverWait(aDriver, 60);
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(element));
    }

    public static void sleep(int second) {
        try {
            Thread.sleep(second * 1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void waitForPageLoad(WebDriver driver) {
        Log.info("#WAIT: waitForPageLoad... ");
        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        // wait for jQuery to load
        ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return ((Long) ((JavascriptExecutor) driver).executeScript("return jQuery.active") == 0);
                } catch (Exception e) {
                    return true;
                }
            }
        };
        //
        ExpectedCondition<Boolean> angularHasFinishedProcessing = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return Boolean.valueOf(((JavascriptExecutor) driver).executeScript("return (window.angular !== undefined) && (angular.element(document).injector() !== undefined) && (angular.element(document).injector().get('$http').pendingRequests.length === 0)").toString());
                } catch (Exception e) {
                    return true;
                }
            }
        };

        try {
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(pageLoadCondition);
            wait.until(jQueryLoad);
            wait.until(angularHasFinishedProcessing);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}


