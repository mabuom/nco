import config.DriverFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.Canvas.CanvasLoginPage;
import pages.Canvas.CanvasPage;
import utils.ReadFileData;

import java.io.File;
import java.util.Properties;

import static utils.ReadFileData.getProp;

public class LoginOnCanvas extends DriverFactory {
    private CanvasLoginPage canvasLoginPage;
    private CanvasPage canvasPage;

    private String instructor1_auto, pass;
    private Properties data;
    @BeforeClass
    public void init(){
        canvasLoginPage = new CanvasLoginPage(webDriver);
        canvasPage = new CanvasPage(webDriver);
        data = getProp(new File("CommonData.properties"));
        instructor1_auto = data.getProperty("instructor1");
        pass = data.getProperty("password");
    }

    @Test
    public void instructorLogin(){
        Object object = ReadFileData.readJsonFile(new File("ISBN1.json"));
        canvasLoginPage.loginCanvas(instructor1_auto, pass);
        canvasPage.goToAppElsevier("course 1", "Nursing Concepts Online for RN 2.0");
    }

}
