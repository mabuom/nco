package SmokeTest;

import config.DriverFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.AssignmentSettingsPage;
import pages.AssignmentsPage;
import pages.Canvas.CanvasLoginPage;
import pages.Canvas.CanvasPage;
import pages.MyResourcesPage;
import utils.CommonMethods;
import utils.Log;
import utils.ReadFileData;

import java.io.File;
import java.util.Properties;

public class smokeTest extends DriverFactory {
    private MyResourcesPage myResourcesPage;
    private CanvasLoginPage canvasLoginPage;
    private CanvasPage canvasPage;
    private AssignmentsPage assignmentsPage;
    private AssignmentSettingsPage assignmentSettingsPage;

    private String username, password, automation_Course, nco_RN_Full;
    private String currentTime = CommonMethods.getCurrentTime();
    private String folderName, developmentConcept;
    private String development_Measuring_Height_and_Weight, development_Head_to_Toe_Assessment;

    private String interrelatedConcepts, exemplars, conceptChapter, skills, caseStudy, reviewQuestions;
    private String skill_Type, quiz_Type;

    private void getCommonData(){
        Properties commonData = ReadFileData.getProp(new File("CommonData.properties"));
        username = commonData.getProperty("auto_instructor1");
        password = commonData.getProperty("password");
        automation_Course = commonData.getProperty("automation_Course1");
        nco_RN_Full = commonData.getProperty("nco_RN_Full");
        developmentConcept = commonData.getProperty("developmentConcept");

        interrelatedConcepts = commonData.getProperty("interrelatedConcepts");
        exemplars = commonData.getProperty("exemplars");
        conceptChapter = commonData.getProperty("conceptChapter");
        skills = commonData.getProperty("skills");
        caseStudy = commonData.getProperty("caseStudy");
        reviewQuestions = commonData.getProperty("reviewQuestions");

        skill_Type = commonData.getProperty("skill_Type");
        quiz_Type = commonData.getProperty("quiz_Type");
    }

    private void getData(){
        Properties data = ReadFileData.getProp(new File("FolderData.properties"));
        folderName = data.getProperty("folderName") + " " + currentTime;
        data = ReadFileData.getProp(new File("SkillName.properties"));
        development_Head_to_Toe_Assessment = data.getProperty("development_Head_to_Toe_Assessment");
        development_Measuring_Height_and_Weight = data.getProperty("development_Measuring_Height_and_Weight");
    }

    @BeforeClass
    public void init(){
        canvasLoginPage = new CanvasLoginPage(webDriver);
        canvasPage = new CanvasPage(webDriver);
        myResourcesPage = new MyResourcesPage(webDriver);
        assignmentsPage = new AssignmentsPage(webDriver);
        assignmentSettingsPage = new AssignmentSettingsPage(webDriver);

        getCommonData();
        getData();

        canvasLoginPage.loginCanvas(username, password);
        canvasPage.goToAppElsevier(automation_Course, nco_RN_Full);
        assignmentsPage.clickMyResources();
    }

    @Test(priority = 1, description = "[NCO] Global nav - 01 - Expanded version - Instructor")
    public void verifyGlobalNav(){
        Log.info("[NCO] Global nav - 01 - Expanded version - Instructor");
        softly.assertThat(assignmentsPage.assignmentsLink.isDisplayed()).isTrue();
        softly.assertThat(assignmentsPage.myResourcesLink.isDisplayed()).isTrue();
        softly.assertThat(assignmentsPage.eaqLink.isDisplayed()).isTrue();
        softly.assertThat(assignmentsPage.getStartedLink.isDisplayed()).isTrue();
        softly.assertThat(assignmentsPage.helpSupportLink.isDisplayed()).isTrue();
        softly.assertThat(assignmentsPage.logoutLink.isDisplayed()).isTrue();

        softly.assertAll();
    }

    @Test(priority = 2, description = "Verify Create Folder")
    public void verifyCreateFolder(){
        Log.info("[NCO] Create Folder - 08 - Create folder in populated My Resources page");
        softly.assertThat(myResourcesPage.isEmptyFirstFolderMessage()).isTrue();
        myResourcesPage.createNewFolder(folderName);
        softly.assertThat(myResourcesPage.isItemName(folderName)).isTrue();
        softly.assertThat(myResourcesPage.isVisible(folderName)).isFalse();

        Log.info("[NCO] Create Folder - 09 - Create folder in My Resources page with instructor blank state");
        softly.assertThat(myResourcesPage.isEmptyFirstFolderMessage()).isFalse();

        softly.assertAll();
    }

    @Test(priority = 3, description = "Verify Visibility Folder")
    public void verifyVisibilityFolder(){
        assignmentsPage.clickMyResources();
        myResourcesPage.createNewFolder(folderName);

        Log.info("[NCO] Visibility settings - 02 - Setting a folder as visible - Instructor");
        myResourcesPage.clickMenuAction(folderName);
        myResourcesPage.clickVisibilitySettings();

        softly.assertThat(myResourcesPage.getFolderNameInVisibilitySettings()).isEqualTo(folderName);
        softly.assertThat(myResourcesPage.isToggle("Off")).isTrue();

        myResourcesPage.clickToggleSwitch();
        softly.assertThat(myResourcesPage.isToggle("On")).isTrue();

        myResourcesPage.clickDone();
        softly.assertThat(myResourcesPage.isVisible(folderName)).isTrue();

        Log.info("[NCO] Visibility settings - 04 - Setting folder as invisible - Instructor");
        myResourcesPage.clickMenuAction(folderName);
        myResourcesPage.clickVisibilitySettings();

        softly.assertThat(myResourcesPage.getFolderNameInVisibilitySettings()).isEqualTo(folderName);
        softly.assertThat(myResourcesPage.isToggle("On")).isTrue();

        myResourcesPage.clickToggleSwitch();
        softly.assertThat(myResourcesPage.isToggle("Off")).isTrue();

        myResourcesPage.clickDone();
        softly.assertThat(myResourcesPage.isVisible(folderName)).isFalse();

        softly.assertAll();
    }

    @Test(priority = 4, description = "Verify Concept on Resource Library and Catalog")
    public void verifyConcept(){
        assignmentsPage.clickMyResources();
        myResourcesPage.createNewFolder(folderName);
        myResourcesPage.clickResource(folderName);
        myResourcesPage.clickAddResource();
        myResourcesPage.clickResourceCatalog(developmentConcept);

        Log.info("[NCO] Case Study - 01 - Verify Case Study available in catalog view");
        Log.info("[NCO] Chapter Links - 01 - Verify Giddens Chapter links are available in catalog view");
        Log.info("[NCO] Exemplar Links - 01 - Verify Exemplar Links are available in catalog view");
        Log.info("[NCO] Review Questions - 01 - Review Questions available in catalog view");
        softly.assertThat(myResourcesPage.isItemName(developmentConcept+" "+interrelatedConcepts)).isTrue();
        softly.assertThat(myResourcesPage.isItemName(developmentConcept+" "+conceptChapter)).isTrue();
        softly.assertThat(myResourcesPage.isItemName(developmentConcept+" "+caseStudy)).isTrue();
        softly.assertThat(myResourcesPage.isItemName(developmentConcept+" "+exemplars)).isTrue();
        softly.assertThat(myResourcesPage.isItemName(developmentConcept+" "+reviewQuestions)).isTrue();

        myResourcesPage.clickClosePopup();
        myResourcesPage.clickAddResource();
        myResourcesPage.clickResourceCatalog(developmentConcept);
        myResourcesPage.addResource(developmentConcept);
        myResourcesPage.clickClosePopup();

        softly.assertThat(myResourcesPage.isItemName(developmentConcept)).isTrue();
        myResourcesPage.clickResource(developmentConcept);

        Log.info("[NCO] Case Study - 04 - View Case Study Content from resource library - Instructor");
        Log.info("[NCO] Chapter Links - 03 - View Giddens Chapter Links Content from resource library - Instructor");
        Log.info("[NCO] Exemplar Links - 04 - View Exemplar Links Content from resource library - Instructor");
        softly.assertThat(myResourcesPage.isItemName(developmentConcept+" "+interrelatedConcepts)).isTrue();
        softly.assertThat(myResourcesPage.isItemName(developmentConcept+" "+conceptChapter)).isTrue();
        softly.assertThat(myResourcesPage.isItemName(developmentConcept+" "+caseStudy)).isTrue();
        softly.assertThat(myResourcesPage.isItemName(developmentConcept+" "+exemplars)).isTrue();
        softly.assertThat(myResourcesPage.isItemName(developmentConcept+" "+reviewQuestions)).isTrue();

        softly.assertAll();
    }

    @Test(priority = 5, description = "Verify Create Assignment Non-Graded")
    public void verifyCreateAssignment_NonGraded(){
        Log.info("[NCO] Create Assignment - 11 - Non-graded assignment");
        assignmentsPage.clickMyResources();
        myResourcesPage.createNewFolder(folderName);
        myResourcesPage.clickResource(folderName);
        myResourcesPage.clickAddResource();
        myResourcesPage.addResource(developmentConcept);
        myResourcesPage.clickClosePopup();

        myResourcesPage.clickResource(developmentConcept);
        myResourcesPage.clickResource(skills);

        myResourcesPage.clickMenuAction(development_Measuring_Height_and_Weight);
        myResourcesPage.clickAssignmentSettings();
        assignmentSettingsPage.clickCreateAssignment();

        softly.assertThat(assignmentSettingsPage.isAssigned(development_Measuring_Height_and_Weight)).isTrue();
        softly.assertThat(assignmentSettingsPage.isGraded(development_Measuring_Height_and_Weight)).isFalse();

        softly.assertAll();
    }


    @Test(priority = 6, description = "Verify Create Assignment Graded")
    public void verifyCreateAssignment_Graded(){
        Log.info("[NCO] Create Assignment - 12 - Clinical Skills - Graded (Scored) assignment");
        assignmentsPage.clickMyResources();
        myResourcesPage.createNewFolder(folderName);
        myResourcesPage.clickResource(folderName);
        myResourcesPage.clickAddResource();
        myResourcesPage.addResource(developmentConcept);
        myResourcesPage.clickClosePopup();

        myResourcesPage.clickResource(developmentConcept);
        myResourcesPage.clickResource(skills);

        myResourcesPage.clickMenuAction(development_Head_to_Toe_Assessment);
        myResourcesPage.clickAssignmentSettings();
        assignmentSettingsPage.clickGradingOptions_Scored();
        assignmentSettingsPage.clickCreateAssignment();

        softly.assertThat(assignmentSettingsPage.isAssigned(development_Head_to_Toe_Assessment)).isTrue();
        softly.assertThat(assignmentSettingsPage.isGraded(development_Head_to_Toe_Assessment)).isTrue();

        softly.assertAll();
    }

    @Test(priority = 7, description = "[NCO] Assignments page - 08 - Non-graded Assignment - Verify all linked assignments are displayed correctly - Instructor")
    public void verifySkillAssignmentDisplayOnAssignmentsPage_Instructor(){
        Log.info("[NCO] Assignments page - 08 - Non-graded Assignment - Verify all linked assignments are displayed correctly - Instructor");
        assignmentsPage.clickMyResources();
        myResourcesPage.createNewFolder(folderName);
        myResourcesPage.clickResource(folderName);
        myResourcesPage.clickAddResource();
        myResourcesPage.addResource(developmentConcept);
        myResourcesPage.clickClosePopup();

        myResourcesPage.clickResource(developmentConcept);
        myResourcesPage.clickResource(skills);
        myResourcesPage.clickMenuAction(development_Measuring_Height_and_Weight);
        myResourcesPage.clickVisibilitySettings();
        myResourcesPage.clickToggleSwitch();
        myResourcesPage.clickDone();

        assignmentsPage.clickAssignments();
        softly.assertThat(assignmentsPage.isAssignment(skill_Type, development_Measuring_Height_and_Weight)).isTrue();
        softly.assertThat(assignmentsPage.isAssignmentGraded(skill_Type, development_Measuring_Height_and_Weight)).isFalse();

        softly.assertAll();
    }

    @Test(priority = 8, description = "Verify Create Assignment Graded")
    public void verifyReviewQuestionOnAssignmentPage(){
        Log.info("[NCO] Review Questions - 04 - Review Questions content view from Assignments page - Instructor");
        assignmentsPage.clickMyResources();
        myResourcesPage.createNewFolder(folderName);
        myResourcesPage.clickResource(folderName);
        myResourcesPage.clickAddResource();
        myResourcesPage.addResource(developmentConcept);
        myResourcesPage.clickClosePopup();

        myResourcesPage.clickResource(developmentConcept);

        myResourcesPage.clickMenuAction(developmentConcept+" "+reviewQuestions);
        myResourcesPage.clickAssignmentSettings();
        assignmentSettingsPage.clickGradingOptions_Scored();
        assignmentSettingsPage.clickCreateAssignment();

        softly.assertThat(assignmentSettingsPage.isAssigned(developmentConcept+" "+reviewQuestions)).isTrue();
        softly.assertThat(assignmentSettingsPage.isGraded(developmentConcept+" "+reviewQuestions)).isTrue();

        assignmentsPage.clickAssignments();
        softly.assertThat(assignmentsPage.isAssignmentGraded(quiz_Type, developmentConcept+" "+reviewQuestions));


        softly.assertAll();
    }

    @Test(priority = 9, description = "[NCO] Assignments page - 09 - Non-graded Assignment -Verify all linked assignments are displayed correctly - Student", enabled = false)
    public void verifySkillAssignmentDisplayOnAssignmentsPage_Student(){
        Log.info("[NCO] Assignments page - 08 - Non-graded Assignment - Verify all linked assignments are displayed correctly - Instructor");
        myResourcesPage.clickMenuAction(development_Measuring_Height_and_Weight);
        myResourcesPage.clickVisibilitySettings();
        myResourcesPage.clickToggleSwitch();
        myResourcesPage.clickDone();

        assignmentsPage.clickAssignments();
        softly.assertThat(assignmentsPage.isAssignmentGraded(skill_Type, development_Measuring_Height_and_Weight)).isFalse();

        softly.assertAll();
    }
}
