import config.DriverFactory;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.AssignmentsPage;
import pages.Canvas.CanvasLoginPage;
import pages.Canvas.CanvasPage;
import pages.MyResourcesPage;
import utils.ReadFileData;

import java.io.File;
import java.util.Properties;

public class verifyConceptContent extends DriverFactory {
    private MyResourcesPage myResourcesPage;
    private CanvasLoginPage canvasLoginPage;
    private CanvasPage canvasPage;
    private AssignmentsPage assignmentsPage;
    private JSONObject getDataFromJson;
    private Properties commonData;

    private String interrelatedConcepts, exemplars, conceptChapter, skills, caseStudy, reviewQuestions;

    @BeforeClass
    public void init(){
        canvasLoginPage = new CanvasLoginPage(webDriver);
        canvasPage = new CanvasPage(webDriver);
        myResourcesPage = new MyResourcesPage(webDriver);
        assignmentsPage = new AssignmentsPage(webDriver);
        getDataFromJson = ReadFileData.readJsonFile(new File("ISBN1.json"));
        commonData = ReadFileData.getProp(new File("CommonData.properties"));

        String username = commonData.getProperty("auto_instructor1");
        String password = commonData.getProperty("password");
        String folder_Name = commonData.getProperty("folder_Name");
        String automation_Course = commonData.getProperty("automation_Course1");
        String nco_RN_Full = commonData.getProperty("nco_RN_Full");

        interrelatedConcepts = commonData.getProperty("interrelatedConcepts");
        exemplars = commonData.getProperty("exemplars");
        conceptChapter = commonData.getProperty("conceptChapter");
        skills = commonData.getProperty("skills");
        caseStudy = commonData.getProperty("caseStudy");
        reviewQuestions = commonData.getProperty("reviewQuestions");

        canvasLoginPage.loginCanvas(username, password);
        canvasPage.goToAppElsevier(automation_Course, nco_RN_Full);
        assignmentsPage.clickMyResources();
        myResourcesPage.clickResource(folder_Name);
    }

    @Test(priority = 1)
    public void verifyDevelopmentConcept(){
        JSONObject jsonObject = getDataFromJson.getJSONObject("Development");
        String conceptNameData = jsonObject.getString("name");
        String interrelatedConceptsData = jsonObject.getString("Interrelated Concepts");
        String conceptChapterData = jsonObject.getString("Concept Chapter");
        String exemplarsData = jsonObject.getString("Exemplars");

        myResourcesPage.clickResource(conceptNameData);

        myResourcesPage.clickResource(conceptNameData + " " + interrelatedConcepts);
        softly.assertThat(myResourcesPage.verifyInterrelatedConcepts(interrelatedConceptsData)).isTrue();
        myResourcesPage.clickExit();

        myResourcesPage.clickResource(conceptNameData + " " + exemplars);
        softly.assertThat(myResourcesPage.getTitleExemplars()).isEqualTo(exemplarsData);
        myResourcesPage.clickExit();

        myResourcesPage.clickResource(conceptNameData + " " + conceptChapter);
        softly.assertThat(myResourcesPage.getURLofConceptChapter()).isEqualTo(conceptChapterData);
        webDriver.close();
        
        softly.assertAll();
    }
}
